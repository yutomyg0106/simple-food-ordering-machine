import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton braisedPorkRamenButton;
    private JButton ramenButton;
    private JButton specialRamenButton;
    private JButton riceButton;
    private JButton seasonedEggRamenButton;
    private JButton chickenOilSobaButton;
    private JTextArea orderedRamensArea;
    private JLabel orderedRamensLabel;
    private JButton checkOutButton;
    private JLabel totalPriceLabel;
    private JButton komatsunaButton;
    private JButton braisedPork1PieceButton;
    private JTextArea orderedToppingsArea;
    private JLabel OrderedToppingsLabel;
    static int totalPrice;
    static int orderedRamenscounter=0, orderedToppingscounter=0;
    ArrayList<String> orderedRamensList = new ArrayList<String>();
    ArrayList<String> orderedToppingsList = new ArrayList<String>();
    void ramenOrder(String ramen, int price) {
        int confirmation =isConfirmationOk(ramen);
        if (confirmation == 0) {
            orderedRamenscounter++;
            String noodlesQuantity=selectNoodlesQuantity();
            orderConfirmationOperation(ramen, noodlesQuantity, price, orderedRamenscounter, orderedRamensList, orderedRamensArea);

            if(ramen.equals("Chicken Oil Soba")) {
                JOptionPane.showMessageDialog(null, "Order for " + ramen + "「Quantity : " + noodlesQuantity + "」received.\n", "Message", JOptionPane.INFORMATION_MESSAGE);
                orderedRamensArea.append(" Ramen customize ⇒ Nothing\n");
            }else{
                JOptionPane.showMessageDialog(null, "Order for " + ramen + "「Quantity : " + noodlesQuantity + "」received.\n Next, please customize your ramen.", "Message", JOptionPane.INFORMATION_MESSAGE);
                ramenCustomize();
            }
        }
    }

    void toppingOrder(String topping, int price) {
        if(orderedRamensList.size()==0){
            JOptionPane.showMessageDialog(null, " Order your ramen first, please.", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int confirmation=isConfirmationOk(topping);
            if (confirmation == 0) {
                orderedToppingscounter++;
                orderConfirmationOperation(topping, "normal", price, orderedToppingscounter, orderedToppingsList, orderedToppingsArea);
                String selectedRamen = toppingToRamen();
                JOptionPane.showMessageDialog(null, "Order for " + topping + " added to " + selectedRamen + " received.", "Message", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }
    int isConfirmationOk(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        return confirmation;
    }
    void orderConfirmationOperation(String food, String quantity, int price, int counter, ArrayList<String> list, JTextArea Area) {
            list.add(counter + ". " + food);
            Area.append(counter + ". " + food + " " + price + " yen , Quantity : " + quantity + "\n");
            totalPrice += price;
            totalPriceLabel.setText("Total " + String.valueOf(totalPrice) + " yen");
    }
    String selectNoodlesQuantity(){
        String noodlesQuantity[]={"normal", "medium (addtional 100 yen)", "large (addtional 200 yen)"};
        int selectedOption;
        selectedOption=JOptionPane.showOptionDialog(null, "How much noodles would you like?", "Choice of quantity of noodles", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, noodlesQuantity, noodlesQuantity[0]);
        if(selectedOption==0){
            return noodlesQuantity[0];
        }else if(selectedOption==1){
            totalPrice+=100;
            totalPriceLabel.setText("Total " + String.valueOf(totalPrice) + " yen");
            return noodlesQuantity[1];
        }else{
            totalPrice+=200;
            totalPriceLabel.setText("Total " + String.valueOf(totalPrice) + " yen");
            return noodlesQuantity[2];
        }
    }
    String toppingToRamen(){
        int selectedOption;
        selectedOption = JOptionPane.showOptionDialog(null, "Which ramen would you like to add your topping to?", "Choice of ramen", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, orderedRamensList.toArray(new String[orderedRamensList.size()]) , orderedRamensList.get(0));
        for (int i = 0; i < orderedRamensList.size(); i++) {
            if (selectedOption == i)
                orderedToppingsArea.append(" added to " + orderedRamensList.get(i) + " \n");
        }
        return orderedRamensList.get(selectedOption);
    }
    void ramenCustomize() {
        String[] howHard = {"tender", "normal", "hard"};
        String[] howThick = {"thin", "normal", "thick"};
        String[] howMuchOil = {"much", "normal", "a little"};
        int selectedOption;

        orderedRamensArea.append(" Ramen customize ⇒ ");
        selectedOption = JOptionPane.showOptionDialog(null, "How hard would you like the ramen?", "Customization of hardness", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, howHard, howHard[1]);
        ramenCustomizePrint(selectedOption, howHard, "How hard : ");
        orderedRamensArea.append(" , ");
        selectedOption = JOptionPane.showOptionDialog(null, "How thick would you like the soup of your ramen?", "Customization of thickness", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, howThick, howThick[1]);
        ramenCustomizePrint(selectedOption, howThick, "How thick : ");
        orderedRamensArea.append(" , ");
        selectedOption = JOptionPane.showOptionDialog(null, "How much oil would you like in your ramen?", "Customization of oil quantity", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, howMuchOil, howMuchOil[1]);
        ramenCustomizePrint(selectedOption, howMuchOil, "How much oil : ");
        orderedRamensArea.append(" \n");
    }
    void ramenCustomizePrint(int selectedOption, String[] custom, String customType) {
        for (int i = 0; i < 3; i++) {
            if (selectedOption == i)
                orderedRamensArea.append(customType + custom[i]);
        }
    }

    public SimpleFoodOrderingMachine() {
        specialRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ramenOrder("Special Ramen", 1400);
            }
        });
        braisedPorkRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ramenOrder("Braised Pork Ramen", 1300);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ramenOrder("Ramen", 900);
            }

        });
        seasonedEggRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ramenOrder("Seasoned Egg Ramen", 1100);

            }
        });
        chickenOilSobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ramenOrder("Chicken Oil Soba", 1200);
            }
        });
        riceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toppingOrder("Rice", 100);
            }
        });
        komatsunaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toppingOrder("Komatsuna", 200);
            }
        });
        braisedPork1PieceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toppingOrder("Braised Pork 1 Piece", 120);
            }

        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(orderedRamensList.size()==0){
                    JOptionPane.showMessageDialog(null, " Order your ramen before checkout, please.", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout ?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);

                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + totalPrice + " yen.", "Message", JOptionPane.INFORMATION_MESSAGE);
                        orderedRamensArea.setText(null);
                        orderedToppingsArea.setText(null);
                        totalPrice = 0; orderedRamenscounter=0; orderedToppingscounter=0;
                        orderedRamensList.clear(); orderedToppingsList.clear();
                        totalPriceLabel.setText("total　0 yen.");
                    }
                }

            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        JOptionPane.showMessageDialog(null, "Welcome to Iekei ramen shop! Please order one or more ramen per person in this shop.", "Message", JOptionPane.INFORMATION_MESSAGE);
    }
}